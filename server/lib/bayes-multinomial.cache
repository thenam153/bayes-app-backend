const tagger = require('./word_tokenizer');
const stopwords = require('./stopwords').stopwords();

class BayesClassifier {

    constructor(smoothing) {
        this.docs = [];
        this.features = {};
        this.featureIDF = {}

        this.classFeatures = this.empty;
        this.classTotals = this.empty;
        this.totalExamples = 0;
        this.smoothing = smoothing || 1.0;
    }

    get empty() {
        return Object.create(null);
    }


    tokenize(text, keepStops) {
        let tokens = [];
        let words = tagger.tag(text);

        if (!keepStops) {
            for (let w of words) {
                // remove stopwords
                if (stopwords.indexOf(w) < 0) {
                    tokens.push(w);
                }
            }
        } else {
            tokens = words;
        }

        // console.log(text, tokens);
        return tokens;
    }

    textToFeatures(text) {
        let features = [];
        if (typeof text === 'string') {
            text = this.tokenize(text, true);
        }

        for (let feature in this.features) {
            features.push(text.filter(_word => _word == feature).length)
            // if (text.indexOf(feature) < 0) {
            //     features.push(0)
            // } else {
            //     features.push(1)
            // }
        }

        return features; // tạo vector feature suất hiện trong this.features
    }

    train() {
        for (let doc of this.docs) {
            let features = this.textToFeatures(doc.text); // vector xuất hiện của các từ
            doc.tf = this.getTf(doc.text);    
            doc.tfidf = {} 
            Object.keys(doc.tf).forEach(key => {
                doc.tfidf[key] = doc.tf[key] * Math.log(this.docs.length /this.featureIDF[key])
            })
            console.log(doc.tfidf)
            this.addExample(features, doc.label);
        }
        // execute instance trainer
        // this.doTrain();
    }
    getTf(words) {
        // console.log(words)
        let _features = {}
        let _totalWords = words.length
        for(let i of words) {
            if(_features[i]) {
                _features[i]++;
            }else {
                _features[i] = 1;
            }
        }
        let _tf = {} 
        Object.keys(_features).forEach(key => {
            _tf[key] =  _features[key] / _totalWords
        })
        // console.log(_features, _tf)
        return _tf
    }

    addDocument(text, classification) {
        if (typeof classification === 'undefined') return;

        if (typeof classification === 'string') {
            classification = classification.trim();
        }

        // word segmentation
        if (typeof text === 'string') {
            text = this.tokenize(text.toLowerCase(), true);   // tokenize // convert to thuong het
        }

        // check input is empty
        if (text.length === 0) return;

        this.docs.push({
            label: classification,
            text: text
        });
        let _featuresIDF = {}
        for (let token of text) {
            this.features[token] = (this.features[token] || 0) + 1;
            if(!_featuresIDF[token]) {
                _featuresIDF[token] = true
                if(Object.keys(this.featureIDF).includes(token)) {
                    this.featureIDF[token]++;
                }else {
                    this.featureIDF[token] = 1;
                }
            }
            
        }
        // console.log(this.features, this.docs, text)
    }
    // observation vector 0 = 1 biểu thị sự xuất hiện của features. label = nhãn phân loại
    addExample(observation, label, tfidf) {
        if (!this.classFeatures[label]) {
            this.classFeatures[label] = this.empty;
            this.classTotals[label] = 0;
        }
        if (observation instanceof Array) {
            this.totalExamples++; // tổng mẫu
            // console.log(label)
            this.classTotals[label]++; // tổng số lần xh của nhãn
            // console.log("\n")
            observation.forEach((o, i) => {
                // console.log(o * (tfidf ? tfidf[Object.keys(this.features)[i]] : 1))
                if (o) {
                    let _word = Object.keys(this.features)[i];
                    // console.log(Object.keys(this.features)[i])
                    if (this.classFeatures[label][i] != undefined) {
                        this.classFeatures[label][i] +=  o * (tfidf ? tfidf[_word] || 0 : 1); // tổng số lần xuất hiện của word i trong các văn bản nhãn 
                    } else {
                        // give an extra for smoothing
                        this.classFeatures[label][i] = o * (tfidf ? tfidf[_word] || 0 : 1); // + giá trị smoothing
                    }
                }else {
                    let _word = Object.keys(this.features)[i];
                    if (this.classFeatures[label][i] != undefined) {
                        this.classFeatures[label][i] +=  o * (tfidf ? tfidf[_word] || 0 : 1); // tổng số lần xuất hiện của word i trong các văn bản nhãn 
                    } else {
                        // give an extra for smoothing
                        this.classFeatures[label][i] = o * (tfidf ? tfidf[_word] || 0 : 1); // + giá trị smoothing
                    }
                }
            })
            // console.log(this.classFeatures[label])
        } else {
            // sparse observation
            for (let value of observation) {
                let _word = Object.keys(this.features)[value];
                if (this.classFeatures[label][value]) {
                    this.classFeatures[label][value] += (tfidf ? tfidf[_word] || 0 : 1);
                } else {
                    // give an extra for smoothing
                    this.classFeatures[label][value] = (tfidf ? tfidf[_word] || 0 : 1) + this.smoothing;
                }
            }
        }
    }

    probabilityOfClass(observation, label) {
        var prob = 1;

        if (observation instanceof Array) {
            var i = observation.length;
            let _totalWords = Object.keys(this.classFeatures[label]).map(i => this.classFeatures[label][i]).reduce((val, _val) => val + _val) + Object.keys(this.classFeatures[label]).length
            // console.log(this.classFeatures[label], Object.keys(this.classFeatures[label]).length)
            while (i--) {
                if (observation[i]) {
                    if(this.classFeatures[label][i] == undefined) continue;
                    var count = this.classFeatures[label][i] + this.smoothing; 
                    // console.log(count, label)
                    // numbers are tiny, add logs rather than take product
                    prob *= count / _totalWords
                }
            }
        } else {
            // sparse observation
            for (var key in observation) {
                var count = this.classFeatures[label][observation[key]] || this.smoothing;
                // numbers are tiny, add logs rather than take product
                prob += Math.log(count / this.classTotals[label]);
            }
        }

        // p(C) * unlogging the above calculation P(X|C)
        // console.log(this.classTotals[label], this.totalExamples)
        prob = (this.classTotals[label] / this.totalExamples) * prob;

        return prob;
    }

    getClassifications(observation) {
        var labels = [];

        for (var className in this.classFeatures) {
            labels.push({
                label: className,
                value: this.probabilityOfClass(observation, className)
            });
        }

        return labels.sort(function (x, y) {
            return y.value - x.value;
        });
    }

    classify(observation) {
        let classifications = this.getClassifications(this.textToFeatures(observation));
        if (!classifications || classifications.length === 0)
            throw 'Not Trained'

        // console.log(observation, classifications);
        console.log(classifications)
        return classifications[0].label;
    }
}

module.exports =  BayesClassifier