var bayesM = require('./bayes-multinomial');
var bayesG = require('./bayes-gausian');
// var classifier = new bayes(0.1)
var classifier = new bayesM()
// var classifier = new bayesG()
classifier.addDocument(`Quản lý chiến dịch tranh cử của ông Biden, bà Jen O'Malley Dillon nói ông Biden đang trên đà trở thành tổng thống tiếp theo của nước Mỹ, theo Guardian`, 'Tranh Cử');
classifier.addDocument(`Theo CNN, Tổng thống Mỹ Donald Trump đang dẫn trước ứng viên Joe Biden ở bang Pennsylvania với khoảng cách 618.000 phiếu bầu. Tuy nhiên, khoảng cách này hoàn toàn có thể bị ông Biden xô đổ trong vài ngày tới.`, 'Tranh Cử');
classifier.addDocument(`Trong trường hợp ông Biden giành chiến thắng ở bang Nevada (6 phiếu đại cử tri), Wisconsin (10 phiếu đại cử tri) và Michigan (16 phiếu đại cử tri), ông Trump sẽ phải trông chờ vào điều kì diệu ở bang Arizona mới có thể giành chiến thắng chung cuộc.`, 'Tranh Cử');
classifier.addDocument(`Ứng viên đảng Dân chủ Joe Biden đã chính thức vượt qua Tổng thống Donald Trump ở bang chiến địa Michigan, theo Fox News. Ông Biden vượt lên dẫn trước ông Trump khoảng 18.000 phiếu, với tỉ lệ 49,4% - 49,0%.`, 'Tranh Cử');
classifier.addDocument(`Hưng phấn có được sau bàn thắng nâng tỉ số lên 2-1 của Quang Hải, phút 52, Thành Lương có một pha nhảy múa giữa hàng thủ Sài Gòn trước khi tung cú sút vô cùng đẳng cấp ở góc hẹp, nâng tỉ số lên 3-1. `, 'Bóng đá');
classifier.addDocument(`MU đang có khởi đầu tệ nhất trên sân nhà trong một mùa giải Ngoại hạng Anh kể từ năm 1972, khi chưa thắng nổi trận nào, hòa 1 và thua tới 3 sau 4 trận sân nhà từ đầu mùa. Họ đang tụt xuống vị trí thứ 15 trên BXH với chỉ 7 điểm sau 6 vòng đấu.`, 'Bóng đá');
classifier.addDocument(`Trận đấu với Everton sẽ có ý nghĩa quyết định tới tương lai của Ole. Nếu MU thua, tôi nghĩ ông ấy sẽ ra đường trong giai đoạn các ĐTQG thi đấu. Một áp lực khủng khiếp đang đè lên vai của Ole".`, 'Bóng đá');
classifier.addDocument(`Phong độ của Messi đang gây nhiều lo lắng cho dư luận bởi anh mới chỉ ghi được có 1 bàn ở giải trong nước, trên thực tế anh ghi 3 bàn trong 8 trận ở mọi giải đấu thì toàn từ chấm 11m. Nhiều người đã đồn đoán về lý do của phong độ này, hoặc do Messi chưa hết lấn cấn sau mùa hè tìm cách rời CLB, hoặc anh không hợp lối chơi và có thể không hợp cả tính cách với HLV Ronald Koeman.`, 'Bóng đá');
classifier.addDocument(`Ở những phút còn lại của hiệp 1, dù đã rất nỗ lực nhưng Salzburg đã không thể có thêm bàn thắng. Ngược lại, pha đá phản lưới nhà của Rasmus Kristensen phút 41 đã khiến 2 đội ra nghỉ với tỷ số tạm thời là 2-1 cho Bayern Munich.`, 'Bóng đá');
 
classifier.train();
 
 
console.log(classifier.classify(`Hà Nội FC nhập cuộc nhanh và chơi tấn công vỗ mặt ngay từ đầu trận khi đối đầu với Sài Gòn ở trận đấu mang tính quyết định trong cuộc đua vô địch V-League 2020. Dù sớm có bàn mở tỉ số ở phút thứ 9, nhưng các chân sút Hà Nội đã phải thẫn thờ trước phong độ đỉnh cao của thủ thành Văn Phong ở những phút sau đó.`));
console.log(classifier.classify(`"Tối qua, tôi còn đang dẫn trước, rất vững chắc, ở nhiều bang quan trọng, gần như tất cả những bang mà đảng Dân chủ kiểm soát, Rồi từng bang một, bắt đầu biến mất một cách thần kỳ khi có những lá phiếu bất ngờ được kiểm. RẤT KÌ LẠ...", ông Trump viết trên mạng xã hội Facebook.`));

// console.log(classifier.toJson())

// ===============================

// classifier.addDocument(`Quản lý chiến dịch tranh cử của ông Biden, bà Jen O'Malley Dillon nói ông Biden đang trên đà trở thành tổng thống tiếp theo của nước Mỹ, theo Guardian`, 'Tranh Cử');
// classifier.addDocument(`Theo CNN, Tổng thống Mỹ Donald Trump đang dẫn trước ứng viên Joe Biden ở bang Pennsylvania với khoảng cách 618.000 phiếu bầu. Tuy nhiên, khoảng cách này hoàn toàn có thể bị ông Biden xô đổ trong vài ngày tới.`, 'Tranh Cử');
// classifier.addDocument(`Trong trường hợp ông Biden giành chiến thắng ở bang Nevada (6 phiếu đại cử tri), Wisconsin (10 phiếu đại cử tri) và Michigan (16 phiếu đại cử tri), ông Trump sẽ phải trông chờ vào điều kì diệu ở bang Arizona mới có thể giành chiến thắng chung cuộc.`, 'Tranh Cử');
// classifier.addDocument(`Ứng viên đảng Dân chủ Joe Biden đã chính thức vượt qua Tổng thống Donald Trump ở bang chiến địa Michigan, theo Fox News. Ông Biden vượt lên dẫn trước ông Trump khoảng 18.000 phiếu, với tỉ lệ 49,4% - 49,0%.`, 'Tranh Cử');
// classifier.addDocument(`Arizona là bang có truyền thống bầu cho đảng Cộng hòa suốt 20 năm qua. Trong hai cuộc bầu cử tổng thống năm 2008 và năm 2012, ông Barack Obama dù đắc cử nhưng cũng không thể giành chiến thắng ở bang này.

// Việc ứng viên đảng Dân chủ Joe Biden chiếm đa số phiếu bầu ở bang Arizona tính đến thời điểm hiện tại được coi là bất ngờ lớn nhất cuộc bầu cử năm nay.

// Đài Fox News dự đoán ông Biden chắc chắn sẽ thắng ở bang Arizona và nhận được 11 phiếu đại cử tri. Điều này khiến cuộc bầu cử càng trở nên khó lường.

// Trên thực tế, chuyện bang Arizona chuyển sang màu xanh là điều mà các cử tri tại đây đã cảm nhận được. Masada Siegel, cư dân ở Arizona, một nhà báo tự do, viết trên tờ Independent, rằng những khu phố ủng hộ đảng Cộng hòa ở bang Arizona, giờ đây tràn ngập biểu ngữ có tên ông Biden và “phó tướng” Kamala Harris.

// Siegel nói người dân bang Arizona đặc biệt coi trọng cựu thượng nghị sĩ John McCain, người đã qua đời vào năm 2018. Siegel vẫn còn nhớ giai đoạn mình là phóng viên truyền hình, được giao nhiệm vụ đi phỏng vấn chính trị gia, nếu may mắn. Các chính trị gia dù không nổi tiếng nhưng cũng luôn cáo bận, chỉ trừ có ông McCain là đồng ý phỏng vấn.

// Điều gì khiến thành trì 20 năm của đảng Cộng hòa bất ngờ quay lưng với ông Trump? - 2
// `, 'Tranh Cử');
// classifier.addDocument(`Hưng phấn có được sau bàn thắng nâng tỉ số lên 2-1 của Quang Hải, phút 52, Thành Lương có một pha nhảy múa giữa hàng thủ Sài Gòn trước khi tung cú sút vô cùng đẳng cấp ở góc hẹp, nâng tỉ số lên 3-1. `, 'Bóng đá');
// classifier.addDocument(`MU đang có khởi đầu tệ nhất trên sân nhà trong một mùa giải Ngoại hạng Anh kể từ năm 1972, khi chưa thắng nổi trận nào, hòa 1 và thua tới 3 sau 4 trận sân nhà từ đầu mùa. Họ đang tụt xuống vị trí thứ 15 trên BXH với chỉ 7 điểm sau 6 vòng đấu.`, 'Bóng đá');
// classifier.addDocument(`Trận đấu với Everton sẽ có ý nghĩa quyết định tới tương lai của Ole. Nếu MU thua, tôi nghĩ ông ấy sẽ ra đường trong giai đoạn các ĐTQG thi đấu. Một áp lực khủng khiếp đang đè lên vai của Ole".`, 'Bóng đá');
// classifier.addDocument(`Phong độ của Messi đang gây nhiều lo lắng cho dư luận bởi anh mới chỉ ghi được có 1 bàn ở giải trong nước, trên thực tế anh ghi 3 bàn trong 8 trận ở mọi giải đấu thì toàn từ chấm 11m. Nhiều người đã đồn đoán về lý do của phong độ này, hoặc do Messi chưa hết lấn cấn sau mùa hè tìm cách rời CLB, hoặc anh không hợp lối chơi và có thể không hợp cả tính cách với HLV Ronald Koeman.`, 'Bóng đá');
// classifier.addDocument(`Ở những phút còn lại của hiệp 1, dù đã rất nỗ lực nhưng Salzburg đã không thể có thêm bàn thắng. Ngược lại, pha đá phản lưới nhà của Rasmus Kristensen phút 41 đã khiến 2 đội ra nghỉ với tỷ số tạm thời là 2-1 cho Bayern Munich.`, 'Bóng đá');


 
// classifier.train();
 
 
// console.log(classifier.classify(`Hà Nội FC nhập cuộc nhanh và chơi tấn công vỗ mặt ngay từ đầu trận khi đối đầu với Sài Gòn ở trận đấu mang tính quyết định trong cuộc đua vô địch V-League 2020. Dù sớm có bàn mở tỉ số ở phút thứ 9, nhưng các chân sút Hà Nội đã phải thẫn thờ trước phong độ đỉnh cao của thủ thành Văn Phong ở những phút sau đó.`));
// console.log(classifier.classify(`"Tối qua, tôi còn đang dẫn trước, rất vững chắc, ở nhiều bang quan trọng, gần như tất cả những bang mà đảng Dân chủ kiểm soát, Rồi từng bang một, bắt đầu biến mất một cách thần kỳ khi có những lá phiếu bất ngờ được kiểm. RẤT KÌ LẠ...", ông Trump viết trên mạng xã hội Facebook.`));


// ======================================

// w1 a
// w2 b
// w3 c
// w4 d
// w5 e
// w6 f
// w7 g

// E1 a b b c e
// E2 b b e f g
// E3 a c d f f
// E4 a f 

// classifier.addDocument(`a b b c e`, 'N');
// classifier.addDocument(`b b e f g`, 'N');
// classifier.addDocument(`a c d f f`, 'S');

// classifier.train()

// console.log(classifier.classify('a g'))