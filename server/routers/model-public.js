const express   = require('express')
const router    = express.Router()
const ctrl      = require('../controllers/model')


router.post('/model/put', ctrl.put)

router.post('/model/train', ctrl.train)

router.post('/model/predict', ctrl.predict)

router.post('/model/get-list-all', ctrl.getListModelAll)

router.post('/model/get-list-public', ctrl.getListModelPublic)

router.post('/model/get-tags', ctrl.getTagOfModel)

module.exports = router