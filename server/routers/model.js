const express   = require('express')
const router    = express.Router()
const ctrl      = require('../controllers/model')


router.post('/model/get-by-key', ctrl.getModelByKey)

router.post('/model/get-by-id', ctrl.getModelById)

router.post('/model/get-list', ctrl.getListModel)

router.post('/model/create', ctrl.createModel)

router.post('/model/update', ctrl.updateModel)

router.post('/model/delete', ctrl.deleteModel)

router.post('/model/refresh-token', ctrl.refreshToken)

// router.post('/model/put', ctrl.put)

// router.post('/model/train', ctrl.train)

// router.post('/model/predict', ctrl.predict)

// router.post('/model/get-list-all', ctrl.getListModelAll)

// router.post('/model/get-list-public', ctrl.getListModelPublic)

router.post('/model/set-status', ctrl.setStatusModel)

// router.post('/model/get-tags', ctrl.getTagOfModel)

module.exports = router