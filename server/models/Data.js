module.exports = (sequelize, DataTypes) => {
    return sequelize.define('data', {
      idData: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      idModel: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      document: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      tag: {
        type: DataTypes.TEXT,
        allowNull: false
      }
    })
  }
  
  module.exports.name = 'Data'