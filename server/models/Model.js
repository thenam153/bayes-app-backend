module.exports = (sequelize, DataTypes) => {
    return sequelize.define('model', {
      idModel: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      idUser: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      modelName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      modelInfo: {
        type: DataTypes.TEXT,
        allowNull: false
      }, 
      config: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      private: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      key: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      token: {
        type: DataTypes.TEXT,
        allowNull: false
      }
    })
  }
  
  module.exports.name = 'Model'