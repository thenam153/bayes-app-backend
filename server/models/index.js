const path = require('path')
const Sequelize = require('sequelize')
const database = require('config').database

const sequelize = new Sequelize(database.dbName, database.dbUser, database.dbPassword, {
    define: {
        freezeTableName: true,
        charset: 'utf8',
        collate: 'utf8_general_ci', 
    },
    host: database.dbHost || 'localhost',
    port: database.dbPort,
    logging: database.logging,
    dialect: database.dialect
})

sequelize.sync({
    // force: true
    force: false
})
.then(() => {
    console.log("SYNC DB SUCCESS")
})
.catch(err => {
    console.log("SYNC DB FAIL", err)
})

let models = ['Data', 'Model', 'User']
models.forEach(model => {
    module.exports[model] = require(path.join(__dirname, model))(sequelize, Sequelize.DataTypes)
})


let associate = function(models) {
    models.User.hasMany(models.Model, {
            foreignKey: {
                name: 'idUser',
                allowNull: false
            },
            onDelete: 'CASCADE'
        });

        models.Model.belongsTo(models.User, {
            foreignKey: {
                name: 'idUser',
                allowNull: false
            }
        });

        models.Model.hasMany(models.Data, {
            foreignKey: {
                name: 'idModel',
                allowNull: false
            },
            onDelete: 'CASCADE'
        });
        models.Data.belongsTo(models.Model, {
            foreignKey: {
                name: 'idModel',
                allowNull: false
            }
        })
}   
associate(module.exports)
module.exports.sequelize = sequelize