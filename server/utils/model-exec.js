const { Router } = require('express')
const { Model, User, Data } = require('../models')
const bayesM = require('../lib/bayes-multinomial');
const bayesG = require('../lib/bayes-gausian');
const response = require('../utils/response');
const { CODES } = require('../utils/codes')

function ModelTrain(req, res) {
    let modelExec = null;
    let config = null;
    let modelSave = null;
    Model.findOne({
        where: {
            key: req.body.key,
            token: req.body.accessToken
        }
    })
    .then(model => {
        if(!model) throw new Error("ERROR")
        modelSave = model
        config = JSON.parse(model.config)
        modelExec = config.type === 'multinomial' ? new bayesM(config) : new bayesG(config)
        // modelExec = new bayesM(config)
        return Data.findAll({
            where: {
                idModel: model.idModel
            }
        })
    })
    .then(async documents => {
        for await(let data of documents) {
            modelExec.addDocument(data.document, data.tag)
        }
        return new Promise(resolve => modelExec.train(resolve))
    })
    .then(() => {
        let modelJson = modelExec.exportModel()
        modelSave.update({
            modelInfo: modelJson
        })
        res.send(new response(CODES.SUCCESS, "", modelJson))
    })
    .catch(err => {
        console.log(err)
    })
}

function ModelPredict(req, res) {
    let modelExec = null;
    let config = null;

    Model.findOne({
        where: {
            key: req.body.key,
            token: req.body.accessToken
        }
    })
    .then(model => {
        if(!model) throw new Error("ERROR")
        config = JSON.parse(model.config)
        modelExec = config.type === 'multinomial' ? new bayesM(config) : new bayesG(config)
        let data = JSON.parse(model.modelInfo)
        modelExec.loadModel(data)
        res.send(new response(CODES.SUCCESS, "", modelExec.getClassify(req.body.document)))
    })
    .then(() => {
        // let modelJson = modelExec.exportModel()
        // res.send(modelJson)
    })
    .catch(err => {
        console.log(err)
    })
}

module.exports = {
    ModelTrain,
    ModelPredict
}