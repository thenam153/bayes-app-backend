const response              = require('../utils/response')
const { CODES }             = require('../utils/codes')
const { Model, User, Data}  = require('../models')
const crypto                = require('crypto')
const model                 = require('../utils/model-exec')

function getModelById(req, res) {
    Model.findOne({
        where: {
            idModel: req.body.idModel
        }
    })
    .then(model => {
        if(!model) {
            throw new Error("NO MODEL")
        }
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function getModelByKey(req, res) {
    console.log(req.body)
    Model.findOne({
        where: {
            key: req.body.key
        }
    })
    .then(model => {
        console.log(model)
        if(!model) {
            throw new Error("NO MODEL")
        }
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function getListModel(req, res) {
    Model.findAll({
        where: {
            idUser: req.decodeUser.idUser
        }
    })
    .then(models => {
        console.log(models)
        res.send(new response(CODES.SUCCESS, "", models))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function getListModelAll(req, res) {
    Model.findAll()
    .then(models => {
        console.log(models)
        res.send(new response(CODES.SUCCESS, "", models))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function getListModelPublic(req, res) {
    Model.findAll({
        where: {
            private: false
        }
    })
    .then(models => {
        console.log(models)
        res.send(new response(CODES.SUCCESS, "", models))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function setStatusModel(req, res) {
    Model.findOne({
        where: {
            idUser: req.decodeUser.idUser,
            idModel: req.body.idModel
        }
    })
    .then(model => {
        return model.update({ private: req.body.private })
    })
    .then(model => {
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.log(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function createModel(req, res) {
    // config = { type: gausian, tdidf: true, smoothing: 1, stopwords: true }
    let config = {
        type: "multinomial",
        tdidf: true,
        smoothing: 1,
        stopwords: true
    }
    // console.log(req.body.config)
    Object.assign(config, req.body.config)
    const data = {
        idUser: req.decodeUser.idUser,
        config: JSON.stringify(config),
        modelName: req.body.modelName,
        modelInfo: "{}",
        key: crypto.randomBytes(20).toString('hex'),
        token: crypto.randomBytes(20).toString('hex'),
        private: req.body.private || true,
        description: req.body.description
    }
    Model.create(data)
    .then(model => {
        console.log(model)
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(_err => {
        console.log(_err)
        res.send(new response(CODES.ERROR_BAD_REQUEST, "", _err))
    })
}

function updateModel(req, res) {
    Model.findOne({
        where: {
            idUser: req.decodeUser.idUser,
            idModel: req.body.idModel
        }
    })
    .then(model => {
        let data = {
            modelName: req.body.modelName,
            config: JSON.stringify(Object.assign(JSON.parse(model.config), req.body.config)),
            description: req.body.description,
            private: req.body.private
        }
        return model.update(data)
    })
    .then(model => {
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.log(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
    
}

function deleteModel(req, res) {
    Model.findOne({
        where: {
            idUser: req.decodeUser.idUser,
            idModel: req.body.idModel
        }
    })
    .then(model => {
        if(!model) {
            throw new Error("NO MODEL")
        }
        return model.destroy()
    })
    .then(model => {
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function refreshToken(req, res) {
    Model.findOne({
        where: {
            idUser: req.decodeUser.idUser,
            idModel: req.body.idModel
        }
    })
    .then(model => {
        if(!model) {
            throw new Error("NO MODEL")
        }
        return model.update({
            token: crypto.randomBytes(20).toString("hex")
        })
    })
    .then(model => {
        res.send(new response(CODES.SUCCESS, "", model))
    })
    .catch(error => {
        console.error(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function put(req, res) {
    console.log(req.body)
    Model.findOne({
        where: {
            key: req.body.key,
            token: req.body.accessToken
        }
    })
    .then(model => {
        if(!model || !req.body.document || !req.body.tag) throw new Error("SOME THING WENT ERROR")

        return Data.create({
            idModel: model.idModel,
            document: req.body.document,
            tag: req.body.tag
        })
    }) 
    .then(data => {
        res.send(new response(CODES.SUCCESS, "", data))
    })
    .catch(error => {
        console.log(error)
        res.send(new response(CODES.ERROR_INVALID_PARAMS, ""))
    })
}

function train(req, res) {
    // goi model exec => add document => train model => export tojson => save vao model
    model.ModelTrain(req, res)
}

function predict(req, res) {
    // goi model exec => predict document => tra ve mang cac du lieu
    model.ModelPredict(req, res)
}

function getTagOfModel(req, res) {
    console.log(req)
    Data.findAll({
        where: {
            idModel: req.body.idModel
        },
        attributes: ['tag'],
        distinct: 'tag',
        group: 'tag'
    })
    .then(data => {
        res.send(new response(CODES.SUCCESS, "", data))
    })
}   

module.exports = {
    getModelById,
    getModelByKey,
    getListModel,
    createModel,
    updateModel,
    deleteModel,
    refreshToken,
    put,
    train,
    predict,
    getListModelAll,
    getListModelPublic,
    setStatusModel,
    getTagOfModel
}